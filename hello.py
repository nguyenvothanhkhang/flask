from flask import Flask, redirect, url_for
from string import digits
app = Flask(__name__)
#set = set([0,1,2,3,4,5,6,7,8,9])
#print(set)
#a = []
@app.route('/kkk/<sentence>')
def hello_name(sentence):
   #for i in sentence:
   #    if not i.isdigit():
   #        a.append(i)
   #p = ''.join(a)
   b = ''.join(i for i in sentence if not i.isdigit())

   return 'No number sentence %s' % b
@app.route('/blog/<int:postID>')
def show_blog(postID):
   return 'Blog Number %d' % postID
if __name__ == '__main__':
   app.run(debug = True)